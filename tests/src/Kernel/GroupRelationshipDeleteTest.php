<?php

namespace Drupal\Tests\subgroup\Kernel;

use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\group\PermissionScopeInterface;
use Drupal\user\RoleInterface;

/**
 * Tests the safety measures regarding group relationship deletion.
 *
 * @group subgroup
 */
class GroupRelationshipDeleteTest extends SubgroupKernelTestBase {

  /**
   * The subgroup handler to use in testing.
   *
   * @var \Drupal\subgroup\Entity\SubgroupHandlerInterface
   */
  protected $subgroupHandler;

  /**
   * The group relationship storage to use in testing.
   *
   * @var \Drupal\group\Entity\Storage\GroupRelationshipStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->subgroupHandler = $this->entityTypeManager->getHandler('group_type', 'subgroup');
    $this->storage = $this->entityTypeManager->getStorage('group_relationship');
  }

  /**
   * Tests whether regular delete access still works.
   */
  public function testRegularDeleteAccess() {
    $group_type = $this->createGroupType();
    $this->createGroupRole([
      'group_type' => $group_type->id(),
      'scope' => PermissionScopeInterface::INSIDER_ID,
      'global_role' => RoleInterface::AUTHENTICATED_ID,
      'permissions' => ['administer members'],
    ]);

    $group = $this->createGroup(['type' => $group_type->id()]);
    $group_relationship = $this->storage->createForEntityInGroup($this->createUser(), $group, 'group_membership', []);
    $this->storage->save($group_relationship);

    $this->assertTrue($group_relationship->access('delete'), 'Group content can be deleted just fine.');
  }

  /**
   * Tests whether deleting group relationship for a leaf is not allowed.
   */
  public function testLeafDeleteAccess() {
    $group_type_parent = $this->createGroupType();
    $group_type_child = $this->createGroupType();
    $this->subgroupHandler->initTree($group_type_parent);
    $this->subgroupHandler->addLeaf($group_type_parent, $group_type_child);
    $group_parent = $this->createGroup(['type' => $group_type_parent->id()]);
    $group_child = $this->createGroup(['type' => $group_type_child->id()]);

    $group_relationship = $this->storage->createForEntityInGroup($group_child, $group_parent, 'subgroup:' . $group_type_child->id(), []);
    $this->storage->save($group_relationship);

    /** @var \Drupal\Core\Access\AccessResultForbidden $access */
    $access = $group_relationship->access('delete', NULL, TRUE);
    $this->assertInstanceOf(AccessResultForbidden::class, $access, 'Group content delete access check returned an AccessResultForbidden.');
    $this->assertEquals('Cannot delete a subgroup group relationship entity directly.', $access->getReason());
  }

  /**
   * Tests whether regular deletes still work.
   */
  public function testRegularDelete() {
    $group_type = $this->createGroupType();
    $group = $this->createGroup(['type' => $group_type->id()]);

    $group_relationship = $this->storage->createForEntityInGroup($this->createUser(), $group, 'group_membership', []);
    $this->storage->save($group_relationship);
    $this->storage->delete([$group_relationship]);

    $this->assertNull($this->storage->load($group_relationship->id()), 'Group content was deleted just fine.');
  }

  /**
   * Tests whether you can delete if the group no longer exists.
   */
  public function testLeafDeleteWithoutGroup() {
    $group_type_parent = $this->createGroupType();
    $group_type_child = $this->createGroupType();
    $this->subgroupHandler->initTree($group_type_parent);
    $this->subgroupHandler->addLeaf($group_type_parent, $group_type_child);
    $group_parent = $this->createGroup(['type' => $group_type_parent->id()]);
    $group_child = $this->createGroup(['type' => $group_type_child->id()]);

    $group_relationship = $this->storage->createForEntityInGroup($group_child, $group_parent, 'subgroup:' . $group_type_child->id(), []);
    $this->storage->save($group_relationship);
    $group_child->delete();

    $this->assertNull($this->storage->load($group_relationship->id()), 'Group content was deleted along with its group just fine.');
  }

  /**
   * Tests whether you cannot delete if the group still exists.
   */
  public function testLeafDeleteWithGroupException() {
    $group_type_parent = $this->createGroupType();
    $group_type_child = $this->createGroupType();
    $this->subgroupHandler->initTree($group_type_parent);
    $this->subgroupHandler->addLeaf($group_type_parent, $group_type_child);
    $group_parent = $this->createGroup(['type' => $group_type_parent->id()]);
    $group_child = $this->createGroup(['type' => $group_type_child->id()]);

    $group_relationship = $this->storage->createForEntityInGroup($group_child, $group_parent, 'subgroup:' . $group_type_child->id(), []);
    $this->storage->save($group_relationship);

    $this->expectException(EntityStorageException::class);
    $this->expectExceptionMessage('Cannot delete a subgroup group relationship entity if its group still exists.');
    $this->storage->delete([$group_relationship]);
  }

}
