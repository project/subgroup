<?php

namespace Drupal\Tests\subgroup\Unit;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\group\Entity\GroupRelationshipTypeInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group\Entity\Storage\GroupRelationshipTypeStorageInterface;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface;
use Drupal\subgroup\Entity\RoleInheritanceStorageInterface;
use Drupal\subgroup\Entity\SubgroupHandlerInterface;
use Drupal\subgroup\Event\GroupTypeLeafEvent;
use Drupal\subgroup\Event\LeafEvents;
use Drupal\subgroup\EventSubscriber\GroupTypeLeafSubscriber;
use Drupal\subgroup\LeafInterface;
use Drupal\subgroup\SubgroupFieldManagerInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the functionality of the group type leaf event subscriber.
 *
 * @coversDefaultClass \Drupal\subgroup\EventSubscriber\GroupTypeLeafSubscriber
 * @group subgroup
 */
class GroupTypeLeafSubscriberTest extends UnitTestCase {

  /**
   * The event subscriber to test.
   *
   * @var \Drupal\subgroup\EventSubscriber\GroupTypeLeafSubscriber
   */
  protected $eventSubscriber;

  /**
   * The entity type manager to run tests on.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy<\Drupal\Core\Entity\EntityTypeManagerInterface>
   */
  protected $entityTypeManager;

  /**
   * The subgroup handler for group types to run tests on.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy<\Drupal\subgroup\Entity\SubgroupHandlerInterface>
   */
  protected $subgroupHandler;

  /**
   * The group relation type manager to run tests on.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy<\Drupal\group\Plugin\Group\Relation\GroupRelationTypeManagerInterface>
   */
  protected $pluginManager;

  /**
   * The subgroup field manager to run tests on.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy<\Drupal\subgroup\SubgroupFieldManagerInterface>
   */
  protected $fieldManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->subgroupHandler = $this->prophesize(SubgroupHandlerInterface::class);
    $this->entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $this->entityTypeManager->getHandler('group_type', 'subgroup')->willReturn($this->subgroupHandler->reveal());
    $this->pluginManager = $this->prophesize(GroupRelationTypeManagerInterface::class);
    $this->fieldManager = $this->prophesize(SubgroupFieldManagerInterface::class);

    $this->eventSubscriber = new GroupTypeLeafSubscriber(
      $this->entityTypeManager->reveal(),
      $this->pluginManager->reveal(),
      $this->fieldManager->reveal()
    );
  }

  /**
   * Tests the getSubscribedEvents() method.
   *
   * @covers ::getSubscribedEvents
   */
  public function testGetSubscribedEvents() {
    $subscribed = GroupTypeLeafSubscriber::getSubscribedEvents();
    $this->assertCount(3, $subscribed);
    $this->assertArrayHasKey(LeafEvents::GROUP_TYPE_LEAF_ADD, $subscribed);
    $this->assertArrayHasKey(LeafEvents::GROUP_TYPE_LEAF_REMOVE, $subscribed);
  }

  /**
   * Tests the onAddLeaf() method for a root leaf.
   *
   * @covers ::onAddLeaf
   */
  public function testOnAddLeafRoot() {
    $original = $this->prophesize(GroupTypeInterface::class)->reveal();
    $group_type = $this->prophesize(GroupTypeInterface::class);
    $group_type->id()->willReturn('foobar');
    $group_type = $group_type->reveal();
    $group_type->original = $original;

    $this->subgroupHandler->isRoot($group_type)->willReturn(TRUE);

    $this->fieldManager->installFields('foobar')->shouldBeCalledOnce();
    $this->pluginManager->clearCachedDefinitions()->shouldBeCalledOnce();
    $this->eventSubscriber->onAddLeaf(new GroupTypeLeafEvent($group_type));
  }

  /**
   * Tests the onAddLeaf() method for a non-root leaf.
   *
   * @covers ::onAddLeaf
   */
  public function testOnAddLeafNonRoot() {
    $original = $this->prophesize(GroupTypeInterface::class)->reveal();
    $group_type = $this->prophesize(GroupTypeInterface::class);
    $group_type->id()->willReturn('foobar');
    $group_type = $group_type->reveal();
    $group_type->original = $original;

    $this->subgroupHandler->isRoot($group_type)->willReturn(FALSE);

    $parent = $this->prophesize(GroupTypeInterface::class)->reveal();
    $this->subgroupHandler->getParent($group_type)->shouldBeCalledOnce();
    $this->subgroupHandler->getParent($group_type)->willReturn($parent);

    $relationship_type = $this->prophesize(GroupRelationshipTypeInterface::class)->reveal();
    $storage = $this->prophesize(GroupRelationshipTypeStorageInterface::class);
    $storage->createFromPlugin($parent, 'subgroup:foobar')->shouldBeCalledOnce();
    $storage->createFromPlugin($parent, 'subgroup:foobar')->willReturn($relationship_type);
    $storage->save($relationship_type)->shouldBeCalledOnce();
    $this->entityTypeManager->getStorage('group_relationship_type')->willReturn($storage->reveal());

    $this->fieldManager->installFields('foobar')->shouldBeCalledOnce();
    $this->pluginManager->clearCachedDefinitions()->shouldBeCalledOnce();
    $this->eventSubscriber->onAddLeaf(new GroupTypeLeafEvent($group_type));
  }

  /**
   * Tests the onRemoveLeaf() method.
   *
   * @covers ::onRemoveLeaf
   */
  public function testOnRemoveLeaf() {
    $original = $this->prophesize(GroupTypeInterface::class)->reveal();

    $leaf = $this->prophesize(LeafInterface::class);
    $leaf->getTree()->willReturn('some_tree');
    $this->subgroupHandler->wrapLeaf($original)->willReturn($leaf->reveal());

    $group_type = $this->prophesize(GroupTypeInterface::class);
    $group_type->id()->willReturn('foobar');
    $group_type = $group_type->reveal();
    $group_type->original = $original;

    $role_inheritance_storage = $this->prophesize(RoleInheritanceStorageInterface::class);
    $role_inheritance_storage->deleteForGroupType($group_type, 'some_tree')->shouldBeCalledOnce();
    $this->entityTypeManager->getStorage('subgroup_role_inheritance')->willReturn($role_inheritance_storage->reveal());

    $relationship_type = $this->prophesize(GroupRelationshipTypeInterface::class)->reveal();
    $storage = $this->prophesize(GroupRelationshipTypeStorageInterface::class);
    $storage->loadByPluginId('subgroup:foobar')->shouldBeCalledOnce();
    $storage->loadByPluginId('subgroup:foobar')->willReturn([$relationship_type]);
    $storage->delete([$relationship_type])->shouldBeCalledOnce();
    $this->entityTypeManager->getStorage('group_relationship_type')->willReturn($storage->reveal());

    $this->fieldManager->deleteFields('foobar')->shouldBeCalledOnce();
    $this->pluginManager->clearCachedDefinitions()->shouldBeCalledOnce();
    $this->eventSubscriber->onRemoveLeaf(new GroupTypeLeafEvent($group_type));
  }

}
