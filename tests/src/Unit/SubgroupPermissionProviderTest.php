<?php

namespace Drupal\Tests\subgroup\Unit;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\group\Plugin\Group\Relation\GroupRelationType;
use Drupal\group\Plugin\Group\RelationHandler\PermissionProviderInterface;
use Drupal\subgroup\Plugin\Group\RelationHandler\SubgroupPermissionProvider;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the Subgroup permission provider.
 *
 * @coversDefaultClass \Drupal\subgroup\Plugin\Group\RelationHandler\SubgroupPermissionProvider
 * @group subgroup
 */
class SubgroupPermissionProviderTest extends UnitTestCase {

  /**
   * The plugin ID to use in testing.
   *
   * @var string
   */
  protected $pluginId = 'subgroup:foo';

  /**
   * The entity type ID to use in testing.
   *
   * @var string
   */
  protected $entityTypeId = 'some_entity';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $entity_type = $this->prophesize(EntityTypeInterface::class);
    $entity_type->entityClassImplements(Argument::any())->willReturn(FALSE);

    $entity_type_manager = $this->prophesize(EntityTypeManagerInterface::class);
    $entity_type_manager->getDefinition($this->entityTypeId)->willReturn($entity_type->reveal());

    $container = $this->prophesize(ContainerInterface::class);
    $container->get('entity_type.manager')->willReturn($entity_type_manager->reveal());
    \Drupal::setContainer($container->reveal());
  }

  /**
   * Tests the overrides to the parent permission provider.
   *
   * @covers ::getPermission
   * @dataProvider getPermissionProvider
   */
  public function testGetPermission($expected, $operation, $target, $scope, $calls_parent) {
    $parent = $this->prophesize(PermissionProviderInterface::class);
    $permission_provider = new SubgroupPermissionProvider($parent->reveal());
    $permission_provider->init($this->pluginId, new GroupRelationType([
      'entity_type_id' => $this->entityTypeId,
      'entity_access' => FALSE,
    ]));

    if ($calls_parent) {
      $parent->getPermission($operation, $target, $scope)->willReturn($expected)->shouldBeCalled();
    }
    else {
      $parent->getPermission($operation, $target, $scope)->shouldNotBeCalled();
    }

    $this->assertSame($expected, $permission_provider->getPermission($operation, $target, $scope));
  }

  /**
   * Data provider for testGetPermission().
   *
   * @return array
   *   A list of testGetPermission method arguments.
   */
  public function getPermissionProvider() {
    $cases = [];

    foreach (['create', 'update', 'delete', 'view', 'foobar'] as $operation) {
      $keys[0] = $operation;

      foreach (['entity', 'relationship'] as $target) {
        $keys[1] = $target;

        foreach (['own', 'any'] as $scope) {
          $keys[2] = $scope;

          $expected = 'whatever-does-not-matter';
          $calls_parent = TRUE;

          if ($target === 'relationship') {
            if ($operation === 'delete') {
              $expected = FALSE;
              $calls_parent = FALSE;
            }
            elseif ($operation === 'create') {
              $expected = FALSE;
              $calls_parent = FALSE;
            }
          }
          elseif ($target === 'entity' && $operation === 'create') {
            $expected = "create $this->pluginId entity";
            $calls_parent = FALSE;
          }

          $cases[implode('-', $keys)] = [
            'expected' => $expected,
            'operation' => $operation,
            'target' => $target,
            'scope' => $scope,
            'calls_parent' => $calls_parent,
          ];
        }
      }
    }

    return $cases;
  }

}
