<?php

namespace Drupal\subgroup\Entity;

use Drupal\Core\Database\Transaction;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageException;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\subgroup\InvalidParentException;
use Drupal\subgroup\InvalidRootException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Subgroup handler for Group entities.
 */
class GroupSubgroupHandler extends SubgroupHandlerBase {

  /**
   * Lock name for the database transaction to mass update a tree.
   */
  const LOCK_NAME = 'subgroup_group_tree_update';

  /**
   * The GroupType subgroup handler.
   *
   * @var \Drupal\subgroup\Entity\SubgroupHandlerInterface
   */
  protected $groupTypeHandler;

  /**
   * The field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $fieldManager;

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The lock backend.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected $lock;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    if (!$instance->storage instanceof SqlEntityStorageInterface) {
      throw new SqlContentEntityStorageException('GroupSubgroupHandler only works with SQL backends.');
    }
    $instance->groupTypeHandler = $container->get('entity_type.manager')->getHandler('group_type', 'subgroup');
    $instance->fieldManager = $container->get('entity_field.manager');
    $instance->database = $container->get('database');
    $instance->lock = $container->get('lock');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function writeLeafData(EntityInterface $entity, $depth, $left, $right, $tree) {
    /** @var \Drupal\group\Entity\GroupInterface $entity */
    $entity
      ->set(SUBGROUP_DEPTH_FIELD, $depth)
      ->set(SUBGROUP_LEFT_FIELD, $left)
      ->set(SUBGROUP_RIGHT_FIELD, $right)
      ->set(SUBGROUP_TREE_FIELD, $tree)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function clearLeafData(EntityInterface $entity, $save) {
    /** @var \Drupal\group\Entity\GroupInterface $entity */
    $entity
      ->set(SUBGROUP_DEPTH_FIELD, NULL)
      ->set(SUBGROUP_LEFT_FIELD, NULL)
      ->set(SUBGROUP_RIGHT_FIELD, NULL)
      ->set(SUBGROUP_TREE_FIELD, NULL);

    if ($save) {
      $entity->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function doInitTree(EntityInterface $entity) {
    /** @var \Drupal\group\Entity\GroupInterface $entity */
    $group_type = $entity->getGroupType();

    if (!$this->groupTypeHandler->isLeaf($group_type)) {
      throw new InvalidRootException('Trying to initialize a tree for a group whose group type is not part of a tree structure.');
    }
    if (!$this->groupTypeHandler->isRoot($group_type)) {
      throw new InvalidRootException('Trying to initialize a tree for a group whose group type is not configured as a tree root.');
    }

    parent::doInitTree($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function doAddLeaf(EntityInterface $parent, EntityInterface $child) {
    assert($parent instanceof GroupInterface);
    assert($child instanceof GroupInterface);

    if ($this->groupTypeHandler->getParent($child->getGroupType())->id() !== $parent->bundle()) {
      throw new InvalidParentException('Provided group cannot be added as a leaf to the parent (incompatible group types).');
    }

    // We do not call the parent here but instead write a few giant queries to
    // avoid performance issues when updating large trees of group entities.
    $this->acquireLock();

    // Get the live parent values to avoid race condition issues.
    $parent_values = $this->getLiveLeafValues($parent);

    // Get all of the IDs that will be affected before we do anything.
    $ids_to_update = $this->storage->getQuery()
      ->condition($this->getRightPropertyName(), $parent_values[SUBGROUP_RIGHT_FIELD], '>=')
      ->condition($this->getTreePropertyName(), $parent_values[SUBGROUP_TREE_FIELD])
      ->accessCheck(FALSE)
      ->execute();

    $transaction = NULL;
    try {
      // Start a transaction for the tree updates.
      $transaction = $this->database->startTransaction(self::LOCK_NAME);

      // Run a few update queries to make room for the new child.
      $this->makeRoomForNewChild($parent_values[SUBGROUP_TREE_FIELD], $parent_values[SUBGROUP_RIGHT_FIELD]);

      // And finally add the new leaf (Using the original right value to work
      // out a 2 unit span).
      $this->writeLeafData(
        $child,
        $parent_values[SUBGROUP_DEPTH_FIELD] + 1,
        $parent_values[SUBGROUP_RIGHT_FIELD],
        $parent_values[SUBGROUP_RIGHT_FIELD] + 1,
        $parent_values[SUBGROUP_TREE_FIELD]
      );

      // Update the leaf data on the passed in parent so that code using the old
      // object reference still gets the new values.
      $parent->set(SUBGROUP_RIGHT_FIELD, $parent_values[SUBGROUP_RIGHT_FIELD] + 2);
    }
    // Something went wrong: We roll back the transaction and release the lock.
    catch (\Exception $e) {
      if ($transaction instanceof Transaction) {
        $transaction->rollBack();
      }
      $this->releaseLock();
      throw $e;
    }

    // All the affected entities need to be cleared from the storage's cache.
    $this->storage->resetCache($ids_to_update);

    $this->releaseLock();
  }

  /**
   * {@inheritdoc}
   */
  protected function doRemoveLeaf(EntityInterface $entity, $save) {
    $leaf = $this->wrapLeaf($entity);

    // If the left and right values are 2 and 3 respectively it means we might
    // be removing the last child of a tree root. In this case, we unset the
    // tree altogether.
    if ($leaf->getLeft() === 2 && $leaf->getRight() === 3 && $this->getDescendantCount($root = $this->storage->load($leaf->getTree())) === 1) {
      $this->clearLeafData($root, TRUE);
      return;
    }

    // We do not call the parent here but instead write a few giant queries to
    // avoid performance issues when updating large trees of group entities.
    $this->acquireLock();

    // Keep local copies of the tree values before we clear it.
    $leaf_right = $leaf->getRight();
    $leaf_tree = $leaf->getTree();

    // Get all of the IDs that will be affected before we do anything.
    $ids_to_update = $this->storage->getQuery()
      ->condition($this->getRightPropertyName(), $leaf_right, '>=')
      ->condition($this->getTreePropertyName(), $leaf_tree)
      ->accessCheck(FALSE)
      ->execute();

    $transaction = NULL;
    try {
      // Start a transaction for the tree updates.
      $transaction = $this->database->startTransaction(self::LOCK_NAME);

      // We can now remove the leaf from the tree.
      $this->clearLeafData($entity, $save);

      // Run a few update queries to clean up excessive space after removal.
      $this->cleanUpTreeAfterRemoval($leaf_tree, $leaf_right);
    }
    // Something went wrong: We roll back the transaction and release the lock.
    catch (\Exception $e) {
      if ($transaction instanceof Transaction) {
        $transaction->rollBack();
      }
      $this->releaseLock();
      throw $e;
    }

    // All the affected entities need to be cleared from the storage's cache.
    $this->storage->resetCache($ids_to_update);

    $this->releaseLock();
  }

  /**
   * Retrieves the live leaf values directly from the database.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to retrieve the leaf values for.
   *
   * @return array
   *   The leaf values, keyed by their field names.
   */
  protected function getLiveLeafValues(GroupInterface $group) {
    assert($this->storage instanceof SqlEntityStorageInterface);
    $table_mapping = $this->storage->getTableMapping();
    $field_storage = $this->fieldManager->getFieldStorageDefinitions('group');

    $query = $this->database->select($table_mapping->getFieldTableName(SUBGROUP_DEPTH_FIELD), 'd');
    $query->condition('d.entity_id', $group->id());
    $query->innerJoin($table_mapping->getFieldTableName(SUBGROUP_LEFT_FIELD), 'l', 'l.entity_id = d.entity_id');
    $query->innerJoin($table_mapping->getFieldTableName(SUBGROUP_RIGHT_FIELD), 'r', 'r.entity_id = d.entity_id');
    $query->innerJoin($table_mapping->getFieldTableName(SUBGROUP_TREE_FIELD), 't', 't.entity_id = d.entity_id');
    $query->addField('d', $table_mapping->getFieldColumnName($field_storage[SUBGROUP_DEPTH_FIELD], 'value'), SUBGROUP_DEPTH_FIELD);
    $query->addField('l', $table_mapping->getFieldColumnName($field_storage[SUBGROUP_LEFT_FIELD], 'value'), SUBGROUP_LEFT_FIELD);
    $query->addField('r', $table_mapping->getFieldColumnName($field_storage[SUBGROUP_RIGHT_FIELD], 'value'), SUBGROUP_RIGHT_FIELD);
    $query->addField('t', $table_mapping->getFieldColumnName($field_storage[SUBGROUP_TREE_FIELD], 'value'), SUBGROUP_TREE_FIELD);

    return $query->execute()->fetchAssoc();
  }

  /**
   * Executes a few update queries to make room for a new child.
   *
   * @param int $tree_id
   *   The tree ID.
   * @param int $parent_right
   *   The parent's current right value.
   */
  protected function makeRoomForNewChild($tree_id, $parent_right) {
    $this->updateAllLeavesAfterRightValue($tree_id, $parent_right, '+ 2');
  }

  /**
   * Executes a few update queries to remove a child from a tree.
   *
   * @param int $tree_id
   *   The tree ID.
   * @param int $child_right
   *   The removed child's right value.
   */
  protected function cleanUpTreeAfterRemoval($tree_id, $child_right) {
    $this->updateAllLeavesAfterRightValue($tree_id, $child_right, '- 2');
  }

  /**
   * Updates all leaves of a tree beyond a certain right value.
   *
   * @param int $tree_id
   *   The tree ID.
   * @param int $right_value
   *   The right value that serves as the cutoff point.
   * @param int $change
   *   The change to enact on the rows: Probably + 2 or - 2.
   */
  protected function updateAllLeavesAfterRightValue($tree_id, $right_value, $change) {
    assert($this->storage instanceof SqlEntityStorageInterface);
    $table_mapping = $this->storage->getTableMapping();
    $field_storage = $this->fieldManager->getFieldStorageDefinitions('group');

    $tree_table = $table_mapping->getFieldTableName(SUBGROUP_TREE_FIELD);
    $tree_value_field = $table_mapping->getFieldColumnName($field_storage[SUBGROUP_TREE_FIELD], 'value');

    // Normally you check for 'right >= parent_right' and 'left > parent_right',
    // But seeing as how a left value should never be identical to a right value
    // we can simplify this by simply checking for >= on both queries.
    $queries = [];
    foreach ([SUBGROUP_RIGHT_FIELD, SUBGROUP_LEFT_FIELD] as $field_name) {
      foreach ($table_mapping->getAllFieldTableNames($field_name) as $table_name) {
        $value_field = $table_mapping->getFieldColumnName($field_storage[$field_name], 'value');
        $queries[] = "
          UPDATE {" . $table_name . "} t1
          INNER JOIN {" . $tree_table . "} t2 ON t1.entity_id = t2.entity_id
          SET t1.{$value_field} = t1.{$value_field} {$change}
          WHERE t1.{$value_field} >= {$right_value}
          AND t2.{$tree_value_field} = {$tree_id}
        ";
      }
    }

    foreach ($queries as $query) {
      $this->database->query($query);
    }
  }

  /**
   * Locks the database for mass tree updates.
   */
  protected function acquireLock() {
    // Use a database lock to avoid two processes updating a tree at the same
    // time. This will use the default lock backend waiting time of 30 seconds.
    $acquired_lock = FALSE;
    do {
      if ($this->lock->lockMayBeAvailable(self::LOCK_NAME) && $this->lock->acquire(self::LOCK_NAME)) {
        $acquired_lock = TRUE;
      }
    } while (!$acquired_lock);
  }

  /**
   * Release the lock used for mass tree updates.
   */
  protected function releaseLock() {
    $this->lock->release(self::LOCK_NAME);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDepthPropertyName() {
    return SUBGROUP_DEPTH_FIELD;
  }

  /**
   * {@inheritdoc}
   */
  protected function getLeftPropertyName() {
    return SUBGROUP_LEFT_FIELD;
  }

  /**
   * {@inheritdoc}
   */
  protected function getRightPropertyName() {
    return SUBGROUP_RIGHT_FIELD;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTreePropertyName() {
    return SUBGROUP_TREE_FIELD;
  }

}
