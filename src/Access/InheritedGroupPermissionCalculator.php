<?php

namespace Drupal\subgroup\Access;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Session\AccountInterface;
use Drupal\flexible_permissions\CalculatedPermissionsItem;
use Drupal\flexible_permissions\PermissionCalculatorBase;
use Drupal\flexible_permissions\RefinableCalculatedPermissionsInterface;
use Drupal\group\Entity\GroupRoleInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\group\PermissionScopeInterface;
use Drupal\subgroup\Entity\RoleInheritanceStorageInterface;
use Drupal\subgroup\Entity\SubgroupHandlerInterface;
use Drupal\user\RoleInterface;

/**
 * Calculates inherited group permissions for an account.
 */
class InheritedGroupPermissionCalculator extends PermissionCalculatorBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The membership loader service.
   *
   * @var \Drupal\group\GroupMembershipLoaderInterface
   */
  protected $membershipLoader;

  /**
   * Constructs a InheritedGroupPermissionCalculator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\group\GroupMembershipLoaderInterface $membership_loader
   *   The group membership loader service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, GroupMembershipLoaderInterface $membership_loader) {
    $this->entityTypeManager = $entity_type_manager;
    $this->membershipLoader = $membership_loader;
  }

  /**
   * {@inheritdoc}
   */
  public function calculatePermissions(AccountInterface $account, $scope) {
    $calculated_permissions = parent::calculatePermissions($account, $scope);
    assert($calculated_permissions instanceof RefinableCalculatedPermissionsInterface);

    if ($scope !== PermissionScopeInterface::INDIVIDUAL_ID) {
      return $calculated_permissions;
    }

    // The inherited permissions need to be recalculated whenever the user is
    // added to or removed from a group.
    $calculated_permissions->addCacheTags(['group_relationship_list:plugin:group_membership:entity:' . $account->id()]);

    $group_types = $this->entityTypeManager->getStorage('group_type')->loadMultiple();
    $group_handler = $this->entityTypeManager->getHandler('group', 'subgroup');
    assert($group_handler instanceof SubgroupHandlerInterface);
    $group_type_handler = $this->entityTypeManager->getHandler('group_type', 'subgroup');
    assert($group_type_handler instanceof SubgroupHandlerInterface);
    $inheritance_storage = $this->entityTypeManager->getStorage('subgroup_role_inheritance');
    assert($inheritance_storage instanceof RoleInheritanceStorageInterface);
    $group_storage = $this->entityTypeManager->getStorage('group');

    // Load all member roles, if any, and key per group type.
    $member_role_ids = [];
    $member_roles = $this->entityTypeManager->getStorage('group_role')->loadByProperties([
      'scope' => PermissionScopeInterface::INSIDER_ID,
      'global_role' => RoleInterface::AUTHENTICATED_ID,
    ]);
    foreach ($member_roles as $member_role) {
      assert($member_role instanceof GroupRoleInterface);
      $member_role_ids[$member_role->getGroupTypeId()] = $member_role->id();
    }

    // Load the users memberships and key by group ID.
    $group_memberships = [];
    foreach ($this->membershipLoader->loadByUser($account) as $group_membership) {
      $group_memberships[$group_membership->getGroupRelationship()->getGroupId()] = $group_membership;
    }

    // Performance boost: Keep track of which tree a group type belongs to.
    $tree_per_group_type = [];
    foreach ($group_memberships as $group_membership) {
      $group = $group_membership->getGroup();
      $group_type_id = $group->bundle();
      $group_type = $group_types[$group_type_id];
      assert($group_type instanceof GroupTypeInterface);

      // If the group is not a leaf, there can be no inheritance.
      if (!$group_handler->isLeaf($group)) {
        // Groups of the root level can becomes leaves, meaning a group which is
        // not a leaf now could be updated to become one. So we need to add the
        // group's cacheable metadata to reflect this.
        $calculated_permissions->addCacheableDependency($group);
        continue;
      }

      // Wrap the leaf and figure out what tree its group type belongs to.
      $leaf = $group_handler->wrapLeaf($group);
      if (!isset($tree_per_group_type[$group_type_id])) {
        $tree_per_group_type[$group_type_id] = $group_type_handler->wrapLeaf($group_type)->getTree();
      }

      // From this point on, any changes in the membership's roles might change
      // what we calculate here. So add the membership as a dependency.
      $calculated_permissions->addCacheableDependency($group_membership);

      // To speed things up, we get the role IDs directly rather than call the
      // getRoles() method on the membership. This is because we do not wish
      // to load the roles but only get the role IDs.
      $role_ids = [];

      // See if the group type has a member role configured and add it.
      if (isset($member_role_ids[$group_type_id])) {
        $role_ids[] = $member_role_ids[$group_type_id];
      }

      // Add the individual roles assigned to the member to the list.
      foreach ($group_membership->getGroupRelationship()->get('group_roles') as $group_role_ref) {
        assert($group_role_ref instanceof EntityReferenceItem);
        $role_ids[] = $group_role_ref->target_id;
      }

      // The inherited permissions need to be recalculated whenever a new role
      // inheritance is set up for this tree or one is removed from it.
      $calculated_permissions->addCacheTags([
        'subgroup_role_inheritance_list:tree:' . $tree_per_group_type[$group_type_id],
      ]);

      // If there are no inheritance entities set up, we can bail out here.
      $inheritances = $inheritance_storage->loadByProperties(['source' => $role_ids]);
      if (empty($inheritances)) {
        continue;
      }

      // If there are inheritance entities set up for this tree, any change to
      // the tree structure might alter the permissions, so we need to add the
      // tree's cache tag to the dependencies.
      $calculated_permissions->addCacheTags($group_handler->getTreeCacheTags($group));

      // We will keep track of all the group IDs that belong to the same tree as
      // the membership's group and are of the target role's group type.
      $group_ids = [];

      /** @var \Drupal\subgroup\Entity\RoleInheritanceInterface $inheritance */
      foreach ($inheritances as $inheritance) {
        // Inheritance entities cannot be updated, so no need to add them as
        // dependencies because adding or removing them already triggers the
        // custom list cache tag added above.
        $target_group_role = $inheritance->getTarget();
        $target_group_type_id = $target_group_role->getGroupTypeId();
        $target_group_type = $group_types[$target_group_type_id];
        assert($target_group_type instanceof GroupTypeInterface);

        // Figure out whether we need to go up or down the tree.
        $target_group_type_depth = $group_type_handler->wrapLeaf($target_group_type)->getDepth();
        $source_group_type_depth = $group_type_handler->wrapLeaf($group_type)->getDepth();
        $search_upwards = $target_group_type_depth < $source_group_type_depth;

        if (!isset($group_ids[$target_group_type_id])) {
          $group_ids[$target_group_type_id] = $group_storage
            ->getQuery()
            ->condition('type', $target_group_type_id)
            ->condition(SUBGROUP_TREE_FIELD, $leaf->getTree())
            ->condition(SUBGROUP_LEFT_FIELD, $leaf->getLeft(), $search_upwards ? '<' : '>')
            ->condition(SUBGROUP_RIGHT_FIELD, $leaf->getRight(), $search_upwards ? '>' : '<')
            ->condition(SUBGROUP_DEPTH_FIELD, $target_group_type_depth)
            ->accessCheck(FALSE)
            ->execute();
        }

        if (!empty($group_ids[$target_group_type_id])) {
          // Add the permissions to the list of permission sets for the targets.
          foreach ($group_ids[$target_group_type_id] as $affected_group_id) {
            // Check if the user is a member of this subgroup already and skip
            // if the target role if it's the member role. This avoids adding
            // the member permissions as individual permissions below, leading
            // to a performance gain on the final query.
            //
            // We could also loop over the existing membership's individual
            // roles and filter out any target roles that already exist on said
            // membership. However, this comes at a performance hit and seeing
            // as how individual roles are added to the individual scope anyhow
            // and we put all inherited permissions into said scope, there's no
            // real gain to be had here compared to what it would cost to load
            // each membership role to compare role IDs.
            if (isset($group_memberships[$affected_group_id]) && $this->isMemberRole($target_group_role)) {
              continue;
            }

            $item = new CalculatedPermissionsItem(
              // We hard-code inherited permissions as individual because there
              // is possibly no real membership in the database, which is a hard
              // requirement for query access to work properly with outsider and
              // insider permissions.
              $scope,
              $affected_group_id,
              $target_group_role->getPermissions(),
              $target_group_role->isAdmin()
            );
            $calculated_permissions->addItem($item);
          }

          // Because we used the role's permissions, it is now a dependency.
          $calculated_permissions->addCacheableDependency($target_group_role);
        }
      }
    }

    return $calculated_permissions;
  }

  /**
   * {@inheritdoc}
   */
  public function getPersistentCacheContexts($scope) {
    if ($scope === PermissionScopeInterface::INDIVIDUAL_ID) {
      // The member roles depend on which memberships you have, for which we do
      // not currently have a dedicated cache context for as it has a very high
      // granularity.
      return ['user'];
    }
    return [];
  }

  /**
   * Determines whether a group role is the classic "member role".
   *
   * @param \Drupal\group\Entity\GroupRoleInterface $group_role
   *   The group role to analyze.
   *
   * @return bool
   *   Whether the provided role is the classic "member role".
   */
  protected function isMemberRole(GroupRoleInterface $group_role) {
    return $group_role->getScope() === PermissionScopeInterface::INSIDER_ID && $group_role->getGlobalRoleId() === RoleInterface::AUTHENTICATED_ID;
  }

}
