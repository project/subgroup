<?php

namespace Drupal\subgroup\Plugin\Group\RelationHandler;

use Drupal\group\Plugin\Group\RelationHandler\PermissionProviderInterface;
use Drupal\group\Plugin\Group\RelationHandler\PermissionProviderTrait;

/**
 * Provides group permissions for the subgroup relation plugin.
 */
class SubgroupPermissionProvider implements PermissionProviderInterface {

  use PermissionProviderTrait;

  /**
   * Constructs a new SubgroupPermissionProvider.
   *
   * @param \Drupal\group\Plugin\Group\RelationHandler\PermissionProviderInterface $parent
   *   The default permission provider.
   */
  public function __construct(PermissionProviderInterface $parent) {
    $this->parent = $parent;
  }

  /**
   * {@inheritdoc}
   */
  public function getPermission($operation, $target, $scope = 'any') {
    if ($target === 'relationship') {
      // A subgroup cannot be put into the global scope.
      if ($operation === 'delete') {
        return FALSE;
      }
      // Cannot add existing groups as subgroups through the UI.
      elseif ($operation === 'create') {
        return FALSE;
      }
    }
    // Normally, the entity create permission requires the plugin to specify
    // entity_access, but this is an exception because we control access without
    // specifying entity_access.
    elseif ($target === 'entity' && $operation === 'create') {
      return "create $this->pluginId entity";
    }
    return $this->parent->getPermission($operation, $target, $scope);
  }

}
