<?php

namespace Drupal\subgroup\Plugin\Group\RelationHandler;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\group\Plugin\Group\RelationHandler\OperationProviderInterface;
use Drupal\group\Plugin\Group\RelationHandler\OperationProviderTrait;

/**
 * Provides operations for the group_membership relation plugin.
 */
class SubgroupOperationProvider implements OperationProviderInterface {

  use OperationProviderTrait;

  /**
   * Constructs a new SubgroupOperationProvider.
   *
   * @param \Drupal\group\Plugin\Group\RelationHandler\OperationProviderInterface $parent
   *   The default operation provider.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation service.
   */
  public function __construct(OperationProviderInterface $parent, TranslationInterface $string_translation) {
    $this->parent = $parent;
    $this->stringTranslation = $string_translation;
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(GroupTypeInterface $group_type) {
    $operations = $this->parent->getOperations($group_type);

    $operations['subgroup-settings'] = [
      'title' => $this->t('Subgroup settings'),
      'url' => new Url('subgroup.settings'),
      'weight' => 99,
    ];

    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  public function getGroupOperations(GroupInterface $group) {
    $operations = $this->parent->getGroupOperations($group);

    $key = implode('-', [
      $this->groupRelationType->id(),
      'create',
      $this->groupRelationType->getEntityBundle(),
    ]);

    if (isset($operations[$key])) {
      $text = $operations[$key]['title'];
      assert($text instanceof TranslatableMarkup);

      $new_string = $text->getUntranslatedString() . ' subgroup';
      $operations[$key]['title'] = $this->t($new_string, $text->getArguments(), $text->getOptions());
    }

    return $operations;
  }

}
